import React from 'react';




function App() {
  return (
    <React.Fragment>
      <header>
        <h1>Wallet</h1>
        <h2>Calculator</h2>
      </header>
      <main>
        <div className="container">
          <section className="total">
            <header className="total__header">
              <h3>Balance</h3>
              <p className="total__balance">0 $</p>
            </header>
            <div className="total__main">
              <div className="total__main-item total__income">
                <h4>Income</h4>
                <p className="total__money total__money-income">
                  +0 $
                </p>
              </div>
              <div className="total__main-item total__expenses">
                <h4>Expenses</h4>
                <p className="total__money total__money-expenses">
                  -0 $
                </p>
              </div>
            </div>
          </section>
          <section className="history">
            <h3>Expenses history</h3>
            <ul className="history__list">
              <li className="history__item history__item_plus">
                Got sallary
                <span className="history__money">+3000$</span>
                <button className="history__delete">x</button>
              </li>
              <li className="history__item history__item_minus">
                Paid the debt
                <span className="history__money">-1000$</span>
                <button className="history__delete">x</button>
              </li>
            </ul>
          </section>
          <section className="operation">
            <h3>New operation</h3>
            <form id="form">
              <label>
                <input type="text" className="operation_fields operation__name" placeholder="Operation name"/>
              </label>
              <label>
                <input type="number" className="operation_fields operation__amount" placeholder="Sum" />
              </label>
              <div className="operation__btns">
                <button type="submit" className="operation__btn operation__btn-substract">Expense</button>
                <button type="submit" className="operation__btn operation__btn-add">Income</button>
              </div>
            </form>
          </section>
        </div>
      </main>
    </React.Fragment>
  );
}

export default App;
